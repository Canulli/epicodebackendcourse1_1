package it.epicode.be.es0804.th1;

public class FakeThread {
    public void start() {
        run();
    }

    private void run() {
        System.out.println("Hello Running");
    }
}
